./configure \
  --prefix=$PREFIX \
  --enable-grm \
  --enable-bin \
  --enable-special \
  --enable-pdt \
  --enable-ngram-fsts \
  --enable-compact-fsts \
  --enable-compress \
  --enable-const-fsts \
  --enable-far \
  --enable-linear-fsts \
  --enable-lookahead-fsts \
  --enable-mpdt \
  --enable-ngram-fsts  

make
make install
